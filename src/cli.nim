import
  sequtils,
  strutils,
  sugar


type
  Mode* = enum
    search, emoji_only, help
  Config* = object
    mode*: Mode
    results*: int
    term*: string
    verbose*: bool


const helptext* = """
emocli v1.0.0

Usage:
  emocli <EMOJI_NAME> [,<EMOJI_NAME>]...
  emocli (search | s) [-n <NUM>] <EMOJI_NAME>

Options:
  -h, --help    Show this text.
  -v, --verbose Print verbose emoji information.
  -n <NUM>      Show <NUM> results when searching [default: 5].

Examples:
  $ emocli penguin
  🐧

  $ emocli search -n 2 dancing
  🕺	man dancing
  💃	woman dancing

  $ emocli wrench, bug
  🔧🐛
"""


func parse_args*(input: seq[string]): Config =
  ## Parse Config object from command-line arguments
  var
    args: seq[string] = input
    mode: Mode = emoji_only
    results: int = 5
    verbose: bool = false
    term: string

  try:
    block config:
      if "-h" in args or "--help" in args:
        mode = help
        break config

      var n_index: int = args.find("-n")
      if n_index != -1:
        results = parse_int(args[n_index + 1])
        mode = search
        args = args[0..n_index-1] & args[n_index+2..^1]

      if "-v" in args or "--verbose" in args:
        verbose = true
        args = args.filter(x => x != "-v" and x != "--verbose")

      if "search" == args[0] or "s" == args[0]:
        mode = search
        args = args[1..^1]

      if len(args) == 0:
        mode = help
        break config
      term = args.join(" ").to_lower()
  except:
    mode = help

  Config(
    mode: mode,
    results: results,
    term: term,
    verbose: verbose
  )
