import
  algorithm,
  os,
  sequtils,
  strformat,
  strutils,
  sugar,
  system
import emo, cli


func osa_distance*(a, b: string): int =
  ## Optimal String Alignment Distance (aka restricted edit distance)
  ## https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance#Algorithm
  var d = new_seq[seq[int]](len(a)+1)
  for i in 0..len(a):
    d[i] = new_seq[int](len(b)+1)

  for i in 0..len(a):
    d[i][0] = i
  for j in 0..len(b):
    d[0][j] = j

  var cost: int
  for i in 1..len(a):
    for j in 1..len(b):
      if a[i-1] == b[j-1]:
        cost = 0
      else:
        cost = 1

      d[i][j] = min([
        d[i-1][j] + 1,       # deletion
        d[i][j-1] + 1,       # insertion
        d[i-1][j-1] +  cost, # substitution
      ])

      if i > 1 and j > 1 and a[i-1] == b[j-2] and a[i-2] == b[j-1]:
        d[i][j] = min([      # transposition
          d[i][j],
          d[i-2][j-2] + 1
        ])

  return d[len(a)][len(b)]


func 🔍*(term: string, n: int): seq[Emoji] =
  ## Return `n` best Emoji for search term `term`
  let results: seq[Emoji] =
    💾.map(e =>
      (e, # score with edit distance from search term
          osa_distance(term, e.name) +
          # reward for containing the term contiguously
          (if e.name.contains(term): -100 else: 0)
      )
    )
    .sorted((a, b) => cmp(a[1], b[1]))
    .map(e => e[0])
  if len(results) < n:
    return results
  else:
    return results[0..n-1]


when is_main_module:
  let 🔧 = parse_args(command_line_params())
  case 🔧.mode
    of help:
      echo helptext

    of emoji_only:
      # print the single best emoji
      # for each comma-separated emoji
      let
        terms: seq[string] = 🔧.term.split(",")
        results: seq[Emoji] = terms.map(term => 🔍(term, 1)[0])
      if 🔧.verbose:
        for r in results:
          echo $r
      else:
        # print the emojis without a trailing newline
        stdout.write results.map(e => e.emoji).join("")

    of search:
      # print search results and their names
      let results = 🔍(🔧.term, 🔧.results)
      for e in results:
        if 🔧.verbose:
          echo $e
        else:
          echo fmt"{e.emoji}{'\t'}{e.name}"
