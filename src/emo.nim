import
  os,
  sequtils,
  strformat,
  strutils,
  sugar


type
  Emoji* = object
    emoji*: string
    name*: string
    status*: string
    version*: string
    codes*: seq[string]


func `$`*(e: Emoji): string =
  fmt"""
{e.emoji} {e.name}
  codepoints: {e.codes.join(" ")}
  status:     {e.status}
  version:    {e.version}
"""


func parse_emoji_string(line: string): string =
  line
    .split('#')[1]
    .split(' ')[1]


func parse_emoji_name(line: string): string =
  line
    .split('#')[1..^1].join(" ")
    .split(' ')[3..^1]
    .join(" ")


func parse_emoji_status(line: string): string =
  line
    .split(';')[1]
    .split(' ')[1]


func parse_emoji_version(line: string): string =
  line
    .split('#')[1 .. ^1].join(" ")
    .split(' ')[2]


func parse_emoji_codes(line: string): seq[string] =
  line
    .split(';')[0]
    .split(' ')
    .filter(s => s != "")


func parse_emoji*(line: string): Emoji =
  ## Make Emoji object from a single line
  Emoji(
    emoji : parse_emoji_string(line),
    name : parse_emoji_name(line),
    status : parse_emoji_status(line),
    version : parse_emoji_version(line),
    codes : parse_emoji_codes(line)
  )


proc read_emojis(): seq[Emoji] =
  ## Read emoji-test.txt file and make Emoji objects.
  ## can be run at compile-time
  get_env("EMOCLI_DATAFILE")
    .static_read()
    .split("\n")
    .filter(line => len(line) > 0 and not line.starts_with('#'))
    .map(parse_emoji)


## Make list of emojis at compile time
const 💾*: seq[Emoji] = read_emojis()
