
# emocli: the emoji picker for your cli

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)
[![pipeline status](https://gitlab.com/AsbjornOlling/emocli/badges/main/pipeline.svg)](https://gitlab.com/AsbjornOlling/emocli/-/commits/main)

**emocli** searches emoji by name, and prints the result to stdout.

## Example Usage

```
$ emocli t-rex
🦖

$ emocli search technologist
🧑‍💻	technologist
👨‍💻	man technologist
👩‍💻	woman technologist
🧑🏿‍💻	technologist: dark skin tone
🧑🏻‍💻	technologist: light skin tone

$ emocli s -n 2 whale
🐋	whale
🐳	spouting whale

$ emocli s cat
🐈	cat
🐱	cat face
🙀	weary cat
🐈‍⬛	black cat
😿	crying cat

$ emocli --verbose polar bear
🐻‍❄️ polar bear
  codepoints: 1F43B 200D 2744 FE0F
  status:     fully-qualified
  version:    E13.0

$ emocli sparkles, santa claus, sparkles
✨🎅✨
```

## Installation

To download the latest build of **emocli**, and make it executable:
```
sudo curl -L -o /usr/local/bin/emocli "https://gitlab.com/AsbjornOlling/emocli/-/jobs/artifacts/main/raw/emocli?job=build"
sudo chmod +x /usr/local/bin/emocli
```

You can also just
[download the executable yourself](https://gitlab.com/AsbjornOlling/emocli/-/jobs/artifacts/main/raw/emocli?job=build)
and put it where you want.

## Building

**emocli** is built using Nix.
Once you have Nix installed, you can you can build it by running `nix build` in the repository root.
