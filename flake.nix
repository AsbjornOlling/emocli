{
  description = "emocli - a command-line emoji picker";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };

        datafile = pkgs.fetchurl {
          url = "https://unicode.org/Public/emoji/15.1/emoji-test.txt";
          sha256 = "sha256-2HbuJJqijqp2z6bfqnAoR6jROwYqpIjUZdA5XugTftk=";
        };

        EMOCLI_DATAFILE = "${datafile}";

        emocli = pkgs.nimPackages.buildNimPackage {
          pname = "emocli";
          version = "1.0.0";
          src = ./.;
          nativeBuildInputs = [ pkgs.musl ];
          nimFlags = [
            "-d:release"
            "--gcc.exe:musl-gcc"
            "--gcc.linkerexe:musl-gcc"
            "--passL:-static"
            "--maxLoopIterationsVM:1000000000"
          ];
          doCheck = true;
          inherit EMOCLI_DATAFILE;
        };

      in {
        packages.default = emocli;
        devShell = pkgs.mkShell {
          buildInputs = [ pkgs.nim pkgs.nimPackages.nimble ];
          inherit EMOCLI_DATAFILE;
        };
        apps.default = {
          type = "app";
          program = "${emocli}/bin/emocli";
        };
        checks.default = emocli;
      });
}
