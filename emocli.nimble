# Package

version       = "1.0.0"
author        = "Asbjørn Olling"
description   = "The emoji picker for your command line"
license       = "EUPL-1.2-or-later"
srcDir        = "src"
bin           = @["emocli"]


# Dependencies

requires "nim >= 1.6.12"
