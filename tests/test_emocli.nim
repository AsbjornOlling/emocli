import
  unittest,
  strutils
import
  ../src/emocli,
  ../src/emo, ../src/cli

suite "Parser tests":
  test "Parse emoji line (🧑‍💻 technologist)":
    let
      technologist = "1F9D1 200D 1F4BB                                       ; fully-qualified     # 🧑‍💻 E12.1 technologist"
      parsed = parse_emoji(technologist)
      expected = Emoji(
        emoji : "🧑‍💻",
        name : "technologist",
        status : "fully-qualified",
        version : "E12.1",
        codes : @["1F9D1", "200D", "1F4BB"]
      )
    assert parsed == expected

  test "Parse emoji line (🏴󠁧󠁢󠁷󠁬󠁳󠁿 wales)":
    let
      wales = "1F3F4 E0067 E0062 E0077 E006C E0073 E007F              ; fully-qualified     # 🏴󠁧󠁢󠁷󠁬󠁳󠁿 E5.0 flag: Wales"
      parsed = parse_emoji(wales)
      expected = Emoji(
        emoji : "🏴󠁧󠁢󠁷󠁬󠁳󠁿",
        name : "flag: Wales",
        status : "fully-qualified",
        version : "E5.0",
        codes : @["1F3F4", "E0067", "E0062", "E0077", "E006C", "E0073", "E007F"]
      )
    assert parsed == expected


suite "String distance tests":
  test "distance(foobar, foobar) == 0":
    assert osa_distance("foobar", "foobar") == 0
    
  test "distance(foobar, foo) == 3":
    assert osa_distance("foobar", "foo") == 3

  test "distance(dog, hotdog) == 3":
    assert osa_distance("dog", "hotdog") == 3

  test "distance(color, colour) == 1":
    assert osa_distance("color", "colour") == 1


suite "Search tests":
  test "search(technologist, 1) == 🧑‍💻":
    let technologist = Emoji(
        emoji : "🧑‍💻",
        name : "technologist",
        status : "fully-qualified",
        version : "E12.1",
        codes : @["1F9D1", "200D", "1F4BB"]
      )
    assert 🔍("technologist", 1) == @[technologist]


suite "Config tests":
  test "parse args '-n 1337 search technologist'":
    let
      parsed = parse_args("-n 1337 --verbose search technologist".split())
      expected = Config(
        results: 1337,
        mode: search,
        term: "technologist",
        verbose: true
      )
    assert parsed == expected
